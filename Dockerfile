FROM node:14.17.0

RUN useradd ernesto

WORKDIR /app_nodejs

COPY package*.json ./

RUN npm install

COPY . .

USER ernesto

CMD [ "node", "index.js" ]

